<?php

?>
<div id="aria-label-block-menu-block-<?php print str_replace('_', '-', $config['delta']); ?>">
<div role="navigation" class="<?php print $classes; ?>" aria-labelledby="aria-label-block-menu-block-<?php print str_replace('_', '-', $config['delta']); ?>">
  <?php print render($content); ?>
</div>
</div>